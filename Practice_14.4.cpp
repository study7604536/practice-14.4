﻿// Practice_14.4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

int main()
{
    std::string Quote = "We can only learn to love by loving";
    std::cout << Quote <<"\n";
    std::cout << Quote.length() << "\n";
    std::cout << Quote[0] << "\n";
    // Поскольку первый символ считается с 0, для последнего символа нужно из длинны вычесть 1
    std::cout << Quote[Quote.length()-1] << "\n";
    return 0;
}

